---
title: How to Design Programs - Data and Data
--- 

Symbols represents symbolic data and are atomic, whereas Strings are compound data.

```racket

(symbol=? 'Hello 'Hello) = true
(string=? "Hello" "Hello") = true

```

Racket also accepts images as symbolic data (I don't have any idea on how it may work, even though it looks kind of fun). 

On chapter 6, it introduces the concept of Structures by using `posn` to represent a pixel.

```racket
(make-posn 3 4)
(define (distance-to-0 a-posn) 
  (sqrt
        (+ (sqr (posn-x a-posn))
           (sqr (posn-y a-posn)))))
```

Every structure should have an `make-` function, as well as, a function to take every attribute back.

```racket
(define-struct entry (name zip phone))
(make-entry 'PeterLee 15270 '606-7771)
```

Types in Racket looks like a prank. `checked functions` looks like a code smell and feels like a hack. However, the author makes a huge effort on trying to get the reader into type reasoning. (I have mixed feelings about these polymorphic functions.)

```racket
(define (distance-to-0 a-pixel)
  (cond
        [(number? a-pixel) a-pixel]
        [(posn? a-pixel) (sqrt
                      (+ (sqr (posn-x a-pixel))
                         (sqr (posn-y a-pixel))))]))

(define (perimeter a-shape)
  (cond
        [(square? a-shape) (* (square-length a-shape) 4)]
        [(circle? a-shape) (* (* 2 (circle-radius a-shape)) pi)]))
```
