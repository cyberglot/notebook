---
title: The first 4 chapters of How to Design Programs
---

After a long while, I have decided to start reading [How to Design Programs](http://htdp.org/). I regret not having tried it yet, as well as, I regret not having learnt how to program using it. It is very joyful and I would recommend it for programming newcomers without thinking. After all, I'm having a better time now in comparison to [Sctructure and Interpretation of Computer Programs](https://mitpress.mit.edu/sicp/full-text/book/book.html). I swear I'll finish SICP any time soon.

Today, I've gotten the first 4 chapters covered. Even thought HtDP is not as thoughtful as SICP, I found it way easier to follow if you have a small to zero knownledge in computer programming. It starts by presenting **functions** and **functions definitions**. It talks about errors and the classes of errors you may stumble upon: syntax error, runtime error and logic error. Unfortunately, nothing can saves from logic errors. Then, it provides a very interesting recipe on how to design small programs, akka functions.

1. Think about the contract of the function, i.e., its type contraints.
2. Think about the purpose of the function.
3. Provide some examples of input and output.
4. Provide its definition, i.e., write the code down.
5. Test it using the examples.

It also adds that to track logic errors, you must build a domain knowlegde of what you're building.

Then it introduces **function composition**, by building an intuition step-by-step. Here's an example of function composition:

```racket
;; function composition
(define (area-of-ring outer inner)
  (- (area-of-disk outer)
     (area-of-disk inner)))

(define (area-of-ring outer inner) 
  (- (* 3.14 (* outer outer))
     (* 3.14 (* inner inner))))
```

At the fourth chapter, it introduces the concept of truthy values, i.e., booleans. And provides the syntax for doing so:
```racket
(and (= x y) (< y z))
````

As well as building conditional functions:
```racket
(define (is-between-5-6? n)
  (and (< 5 n) (< n 6)))
```

It finishes by providing a revisited recipe to building functions, but now conditional functions:

1. Determine the distinct situations a function deals with 
2. Provide some examples of input and output.
3. Provide the conditionals definition, i.e., write the code down related to `cond`-clauses.
4. Provide the body of the `cond`-clauses.
