---
title: How to Design Programs - Syntax, Semantics and Composed Data
--- 

The 8th chapter of [How to Design Programs](http://htdp.org/) introduces the definition of *Syntax*

> A sentence in a programming language is an expression or a function; the language's grammar dictates how to form complete sentences from words. Programmers use the terminology *SYNTAX* to refer to the vocabularies and grammars of programming languages.

and *Semantics*.

> To determine whether or not a sentence is meaningful, we must study the MEANING, or SEMANTICS, of words and sentences. [...] we discuss the meaning of Scheme programs through an extension of the familiar laws of arithmetic and algebra.

Then, it presents the abstract language of Racket by covering all content seen up until now.

List
===

List is a composed data structure `cons`tructed by a value and an `empty`, nested with another List struture.

```racket
(cons 'Earth (cons 'Venus (cons 'Mercury empty)))
```

`first` is used to get the value from a list. As well as, `rest` is used to get the nested list.
```racket
  (first (cons 10 empty))
= 10

  (rest (cons 10 empty))
= empty

  (first (rest (cons 10 (cons 22 empty))))
= (first (cons 22 empty))
= 22
```

`cons` is analogous to the `make-*` function, but it is a checked funtion.

`constains?` is a function to check whether a List constains a symbol.
```racket
(define (contains-doll? a-symbol a-list-of-symbols)
  (cond
    [(empty? a-list-of-symbols) false]
    [else (cond
            [(symbol=? (first a-list-of-symbols) a-symbol) true]
            [else (contains-doll? (rest a-symbol a-list-of-symbols))])]))
```

It suggests a template for building recursive functions:
> For a recursive data definition to be valid, it must satisfy two conditions. First, it must contain at least two clauses. Second, at least one of the clauses must not refer back to the definition.

```racket
(define (sum a-list-of-nums)
  (cond
    [(empty? a-list-of-nums) 0]
    [else (+ (first a-list-of-nums) (sum (rest a-list-of-nums)))]))
```

And functions that produces lists:
```racket
;; wage : number -> number
;; to compute the total wage (at $12 per hour) ;; of someone who worked for h hours
(define (wage h)
  (* 12 h))

;; hours->wages : list-of-numbers -> list-of-numbers
;; to create a list of weekly wages from a list of weekly hours (alon) 
(define (hours->wages alon)
  (cond
    [(empty? alon) empty]
    [else (cons (wage (first alon)) (hours->wages (rest alon)))]))
```
At the end, we can also build list of structures and other compound data:
```racket
(define-struct ir (name price))

(cons (make-ir 'robot 22.05) 
  (cons (make-ir 'doll 17.95)
    empty))
    
(define (sum an-inv)
  (cond
    [(empty? an-inv) 0]
    [else (+ (ir-price (first an-inv)) (sum (rest an-inv)))]))
```
